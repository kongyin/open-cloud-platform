package com.ocp.server.user;

import com.ocp.common.lb.annotation.EnableFeignInterceptor;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 用户服务
 * @author kong
 * @date 2021/08/21 19:24
 * blog: http://blog.kongyin.ltd
 */
@EnableTransactionManagement
@EnableFeignInterceptor
@EnableDiscoveryClient
@SpringBootApplication
@EnableEncryptableProperties
public class UserApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class,args);
    }
}
