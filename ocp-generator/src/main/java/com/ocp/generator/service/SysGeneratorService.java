package com.ocp.generator.service;

import com.ocp.generator.utils.PageUtils;
import com.ocp.generator.utils.Query;

/**
 * @author kong
 * @date 2021/11/11 19:56
 * blog: http://blog.kongyin.ltd
 */
public interface SysGeneratorService {

    PageUtils queryList(Query query);

    byte[] generatorCode(String[] split);
}
