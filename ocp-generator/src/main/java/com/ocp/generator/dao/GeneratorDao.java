package com.ocp.generator.dao;

import java.util.List;
import java.util.Map;

/**
 *
 * 数据库接口
 * @author kong
 * @date 2021/11/11 19:33
 * blog: http://blog.kongyin.ltd
 */
public interface GeneratorDao {

    List<Map<String, Object>> queryList(Map<String, Object> map);

    Map<String, String> queryTable(String tableName);

    List<Map<String, String>> queryColumns(String tableName);
}
