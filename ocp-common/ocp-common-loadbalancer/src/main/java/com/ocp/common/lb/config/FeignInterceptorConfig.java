package com.ocp.common.lb.config;

import cn.hutool.core.util.StrUtil;
import com.ocp.common.constant.MessageHeaderConstants;
import com.ocp.common.context.TenantContextHolder;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;

/**
 * feign拦截器，只包含基础数据
 * @author kong
 * @date 2021/08/04 21:10
 * blog: http://blog.kongyin.ltd
 */
public class FeignInterceptorConfig {

    /**
     * 使用feign client访问别的微服务时，将上游传过来的client等信息放入header传递给下一个服务
     */
    @Bean
    public RequestInterceptor requestInterceptor(){
        return template -> {
            //传递client
            String tenant = TenantContextHolder.getTenant();
            if (StrUtil.isNotEmpty(tenant)) {
                template.header(MessageHeaderConstants.TENANT_HEADER, tenant);
            }
        };
    }
}
