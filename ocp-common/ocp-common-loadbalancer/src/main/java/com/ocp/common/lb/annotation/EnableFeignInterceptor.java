package com.ocp.common.lb.annotation;

import com.ocp.common.lb.config.FeignHttpInterceptorConfig;
import com.ocp.common.lb.config.FeignInterceptorConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启feign拦截器传递数据给下游服务，包含基础数据和http的相关数据
 * @author kong
 * @date 2021/09/08 20:30
 * blog: http://blog.kongyin.ltd
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({FeignHttpInterceptorConfig.class, FeignInterceptorConfig.class})
public @interface EnableFeignInterceptor {
}
