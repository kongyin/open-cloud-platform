package com.ocp.common.lb.annotation;

import com.ocp.common.lb.config.FeignInterceptorConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author kong
 * @date 2021/09/08 20:37
 * blog: http://blog.kongyin.ltd
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({FeignInterceptorConfig.class})
public @interface EnableBaseFeignInterceptor {
}
