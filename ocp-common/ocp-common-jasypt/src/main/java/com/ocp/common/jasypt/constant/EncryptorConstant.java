package com.ocp.common.jasypt.constant;

/**
 * 加密常量
 * @author kongyin
 * @date 2022/02/18 20:18
 */
public interface EncryptorConstant {

    /**
     * 密钥盐
     */
    String PASSWORD = "kong";

    /**
     * 自定义加密器默认 bean 名称
     * 可通过  配置属性 jasypt.encryptor.bean=xxx 替换
     */
    String BEAN_NAME = "jasyptStringEncryptor";
}
