package com.ocp.common.jasypt;

import com.ocp.common.jasypt.encryptor.CustomStringEncryptor;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.context.annotation.Import;

/**
 * Jasypt 自动配置
 * @author kongyin
 * @date 2022/02/18 19:05
 */
@EnableEncryptableProperties
@Import(CustomStringEncryptor.class)
public class JasyptAutoConfigure {
}
