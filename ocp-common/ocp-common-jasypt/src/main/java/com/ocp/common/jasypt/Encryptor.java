package com.ocp.common.jasypt;

import com.ocp.common.jasypt.constant.EncryptorConstant;
import org.jasypt.util.text.BasicTextEncryptor;

/**
 * 加密器
 * @author kongyin
 * @date 2022/02/18 19:09
 */
public class Encryptor {
    public static void main(String[] args) {
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        //加密所需的密钥
        textEncryptor.setPassword(EncryptorConstant.PASSWORD);
        //要加密的数据
        String password = textEncryptor.encrypt("root");
        System.out.println(password);
    }
}
